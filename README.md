## Presentation Styles

Themes are written in css, the provided versions are from the `Reveal.js` original repository, more themes can be developed using the raw css or you can use the `reveal.js` way to create a new theme, shown as below:



#### Creating a Theme

To create a new theme, start by duplicating a ```.scss``` file in `source` folder. It will be automatically compiled by Grunt from Sass to CSS when you run `npm run build -- themes` in the root folder.

Each theme file does four things in the following order:

1. **Include `template/mixins.scss`**
   Shared utility functions.

2. **Include `template/settings.scss`**
   Declares a set of custom variables that the template file (step 4) expects. Can be overridden in step 3.

3. **Override theme settings**
   This is where you override the default theme. Either by specifying variables or by adding any selectors and styles you please.

4. **Include `template/theme.scss`**
   The template theme file which will generate final CSS output based on the currently defined variables.
