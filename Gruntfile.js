/* global module:false */
module.exports = function(grunt) {
    var port = grunt.option('port') || 8000;
    var root = grunt.option('root') || '.';

    if (!Array.isArray(root)) root = [root];

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            themes: {
                expand: true,
                cwd: 'source',
                src: ['*.sass', '*.scss'],
                dest: '.',
                ext: '.css'
            }
        },
        watch: {
            theme: {
                files: [
                    'source/*.sass',
                    'source/*.scss',
                    'template/*.sass',
                    'template/*.scss'
                ],
                tasks: 'themes'
            }
        }
    });

    // Dependencies
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-sass' );
    // Theme CSS
    grunt.registerTask( 'themes', [ 'sass:themes' ] );

};
